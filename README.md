# 🚀 Mini Project Contact Submission

Welcome to the mini project submission repository! This project is aimed at providing a delightful experience for managing contacts through a web application built with Vue.js and Laravel, featuring an admin generator powered by Craftable Laravel.

## 📋 Table of Contents

- [👋 Introduction](#-introduction)
- [✨ Features](#-features)
- [🛠️ Setup](#️-setup)
- [🚀 Usage](#-usage)
- [🤝 Contributing](#-contributing)
- [📝 License](#-license)

## 👋 Introduction

This project is all about simplifying contact management with a touch of elegance. Vue.js and Laravel join forces to provide a smooth frontend and a robust backend, while Craftable Laravel adds a sprinkle of magic with its admin generator capabilities.

## ✨ Features

- 📝 Create, Read, Update, and Delete contacts
- 🖥️ User-friendly interface crafted with Vue.js
- ⚙️ Efficient backend powered by Laravel
- 🛠️ Admin panel generated seamlessly using Craftable Laravel

## 🛠️ Setup

Get started with the project in no time! Follow these steps:

1. **Clone the repository** to your local machine:

   ```
   git clone <repository_url>
   ```

2. **Navigate**  to the cloned directory:

    ```
    cd <project_directory>
    ```

3. Install dependencies for both the main app and admin generator:

    ```
    # For main app
    cd main-app
    npm install
    ```

# For admin generator

    ```
    cd admin-generator
    composer install
    ```

Configure the database settings in the .env file for Laravel.

Migrate the database schema:
   
    ```
    # For main app
    cd main-app
    php artisan migrate
    ```

# For admin generator

    ```
    cd admin-generator
    php artisan migrate
    ```

Compile assets for the main app:

    ```
    cd main-app
    npm run dev
    ```

Serve the applications:

    ```
    # For main app
    cd main-app
    php artisan serve
    ```

# For admin generator

    ```
    cd admin-generator
    php artisan serve
    ```

Access the applications in your browser:

Main app: http://localhost:8000

Admin generator: http://localhost:8001

🚀 Usage
Access the main app to perform CRUD operations on contacts.
Utilize the admin generator to manage administrative tasks efficiently.
